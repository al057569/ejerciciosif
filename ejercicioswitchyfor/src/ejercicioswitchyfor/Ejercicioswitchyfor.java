/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioswitchyfor;
import java.util.Scanner;
/**
 *
 * @author Karen Perez
 */
public class Ejercicioswitchyfor {

    /**
     * @param args the command line arguments
     */
    
    static void imprimirMensaje (String sMensaje){
    System.out.println(sMensaje);
    }
    
    static void encabezado(){
    imprimirMensaje("              Universidad Autonoma de Campeche");
    imprimirMensaje("                  Facultad de ingenieria");
    imprimirMensaje("            Ingenieria en Sistemas Computacionales");
    imprimirMensaje("                  Lenguaje de Programacion I");
    separador(true);
    }
    
    static void separador (boolean tipo){
    if (tipo) {
        imprimirMensaje("_____________________________________________________________");
    } else {
    imprimirMensaje("=============================================================");
    imprimirMensaje("                Karen Estefania Perez Perez");
    imprimirMensaje("                     Matricula: 57569");
}
}
    
    static void semana (int opcionsubmenu){
        switch (opcionsubmenu) {
            case 1:
                separador(true);
                imprimirMensaje("1.- Lunes ");
                imprimirMensaje("Dia habil");
                break;
            case 2:
                separador(true);
                imprimirMensaje("2.- Martes");
                imprimirMensaje("Dia habil");
                break;
            case 3:
                separador(true);
                imprimirMensaje("3.- Miercoles");
                imprimirMensaje("Dia habil");
                break;
            case 4:
                separador(true);
                imprimirMensaje("4.- Jueves");
                imprimirMensaje("Dia habil");
                break;
            case 5:
                separador(true);
                imprimirMensaje("5.- Viernes");
                imprimirMensaje("Dia habil");
                break;
            case 6:
                separador(true);
                imprimirMensaje("6.- Sabado");
                imprimirMensaje("Fin de semana");
                break;
            case 7:
                separador(true);
                imprimirMensaje("7.- Domingo");
                imprimirMensaje("Fin de semana");
                break;
            default :
                imprimirMensaje("No es un dia de la semana");          
                break;
    }
    }
    
    static void personajes () {
        separador(true);
        imprimirMensaje("2.- Personajes de Star Wars");
                String []aPersonajes = {"Luke Skywalker","R2-D2","C-3PO", "Darth Vader", "Leia Organa","Owen Lars",
                    "Beru Whitesun lars","R5-D4","Biggs Darklighter","Obi-Wan Kenobi","Yoda","Jek Tono Porkins",
                    "Jabba Desilijic Tiure","Han Solo","Chewbacca","Anakin Skywalker", "172","96","167", "202","150",
                    "178","165","97","183","182","66","180","175","180","228","188","male","n/a","n/a","male","female",
                    "male","female","n/a","male","male","male","male","hermaphrodite","male","male","male"};
                String [][]aInfoJunta= {{aPersonajes[0],aPersonajes[16],aPersonajes[32]},{aPersonajes[1],aPersonajes[17],aPersonajes[33]},
                    {aPersonajes[2],aPersonajes[18],aPersonajes[34]},{aPersonajes[3],aPersonajes[19],aPersonajes[35]},
                    {aPersonajes[4],aPersonajes[20],aPersonajes[36]},{aPersonajes[5],aPersonajes[21],aPersonajes[37]},
                    {aPersonajes[6],aPersonajes[22],aPersonajes[38]},{aPersonajes[7],aPersonajes[23],aPersonajes[39]},
                    {aPersonajes[8],aPersonajes[24],aPersonajes[40]},{aPersonajes[9],aPersonajes[25],aPersonajes[41]},
                    {aPersonajes[10],aPersonajes[26],aPersonajes[42]},{aPersonajes[11],aPersonajes[27],aPersonajes[43]},
                    {aPersonajes[12],aPersonajes[28],aPersonajes[44]},{aPersonajes[13],aPersonajes[29],aPersonajes[45]},
                    {aPersonajes[14],aPersonajes[30],aPersonajes[46]},{aPersonajes[15],aPersonajes[31],aPersonajes[47]}};
      
        
        for (int i=0; i<16; i++){
           if (i>=0)System.out.print(" | ");
        for (int j=0; j<3; j++){
            System.out.print(aInfoJunta [i][j]);
            
          if (j>=0) System.out.print(" | ");
          
        }
        System.out.println(" ");
        }
    }
    
    static void media(){
        separador(true);
        imprimirMensaje("3.- Media de la suma de los numeros del 0 hasta el numero deseado");
        int Promedio = 0;
        int Suma = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Teccle hasta que numero quiere saber la media: ");
        int Numero = sc.nextInt();
        for (int i=0; i<=Numero; i++){
        imprimirMensaje("Valores: " +i);
        Suma= Suma + i;
        }
        imprimirMensaje("La suma total es: " + Suma);
        Promedio = Suma/Numero;
        imprimirMensaje("La media de los numeros es: " + Promedio);
        

       }
    
     static void submenu(int opcionmenu){
        switch (opcionmenu){
            case 1:
                separador(true);
                imprimirMensaje("1.- Lunes ");
                imprimirMensaje("2.- Martes ");
                imprimirMensaje("3.- Miercoles");
                imprimirMensaje("4.- Jueves");
                imprimirMensaje("5.- Viernes");
                imprimirMensaje("6.- Sabado");
                imprimirMensaje("7.- Domingo");
                Scanner entrada = new Scanner( System.in );
                System.out.print("Teccle la opción del dia deseado: ");
                int iOpcion = entrada.nextInt();
                semana(iOpcion);
                break;
            case 2:
                personajes();
                break;
            case 3:
                media();
                break;
            default :
                imprimirMensaje("No es un numero disponible del menu");          
                break;
        }
    }

    
    static void menu(){
        imprimirMensaje("1.- Dias de la semana que son habiles o fin de semana");
        imprimirMensaje("2.- Personajes de Star Wars");
        imprimirMensaje("3.- Media de la suma de los numeros del 0 hasta el numero deseado");
        imprimirMensaje("  ");
        
        Scanner entrada = new Scanner( System.in );
        System.out.print("Teccle la opción deseada: ");        
        int iOpcion = entrada.nextInt();
        submenu(iOpcion);
    }

    
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        menu();
        separador(false);
    }
    
}
