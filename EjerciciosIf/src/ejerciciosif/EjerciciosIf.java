/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosif;
import java.util.Scanner;

/**
 *
 * @author Karen Perez
 */
public class EjerciciosIf {
    /**
     * @param args the command line arguments
     */
    
    static void imprimirMensaje(String sMensaje){
    System.out.println(sMensaje);
    }
    
    static void encabezado (){
    imprimirMensaje ("      Universidad Autonoma de Campeche");
    imprimirMensaje("           Facultad de ingenieria");
    imprimirMensaje ("    Ingenieria en Sistemas Computacionales");
    imprimirMensaje("           Lenguaje de programación I");
    separador(true);
    }
    
    static void separador (boolean tipo){
    if(tipo){
    imprimirMensaje("_______________________________________________________");
    } else{
    imprimirMensaje("======================================================");
    imprimirMensaje("           Karen Estefanía Pérez Pérez");
    imprimirMensaje("                Matricula: 57569");
    }  
    }
    
    static void positivonegativo(){
    Scanner entrada = new Scanner( System.in );
    System.out.print("Introduzca un numero: ");        
    int iValor = entrada.nextInt();
    if (iValor >= 0){
        imprimirMensaje("Es positivo");     
    } else {
           imprimirMensaje("Es negativo");                        
        }    
    }
    
    static void temperatura(){
    Scanner entrada = new Scanner (System.in);
    imprimirMensaje("Introduzca la temperatura en centigrados del día:");
    int iTemperatura = entrada.nextInt();
    if (iTemperatura <= 10) {
        imprimirMensaje("Frio");
    } else if (10 < iTemperatura && iTemperatura <= 20){
        imprimirMensaje ("Nublado");
    } else if (20 < iTemperatura && iTemperatura <= 30){
        imprimirMensaje("Caluroso");
    } else {
        imprimirMensaje("Tropical");
    }
}
    
    static void submenu (int opcion){
    if (opcion == 1){
        imprimirMensaje("1.- Numero positivo o negativo");
        separador(true);
        positivonegativo();
    }
    if (opcion == 2){
        imprimirMensaje("2.- Temperatura del dia");
        separador(true);
        temperatura();
    }
    }

    static void menu(){
    imprimirMensaje("1.- Numero positivo o negativo");
    imprimirMensaje("2.- Temperatura del día");
    Scanner entrada = new Scanner( System.in );
    System.out.print("Teccle la opción deseada: ");        
    int iOpcion = entrada.nextInt();
    submenu(iOpcion);
    }

    
    
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        menu();
        separador(false);
    }
    
}
